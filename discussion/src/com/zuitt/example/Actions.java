package com.zuitt.example;


//[] Abstraction
//Declares method without providing their implementation. It serves
public interface Actions {
 public void sleep();
 public void run();

}
