package com.zuitt.example;

import java.util.Scanner;

public class Main {
    public static  void main(String[] args){
        //Driver
        Driver newDriver = new Driver();
//        Scanner input = new Scanner();
        newDriver.setName("alpha");
//        System.out.print(newDriver.name);
        System.out.println(newDriver.getName());

        Driver newDriver2 = new Driver("joy");
        System.out.println(newDriver2.getName());

        //Car
        Car myCar = new Car();

        System.out.println("this car is driven by: " + myCar.getDriver());
        Car myCar2 = new Car("Model","Tesla", 2020);
        System.out.println(myCar2.getCarName());
        System.out.println(myCar2.getBrand());
        System.out.println(myCar2.getYearOfMade());
        System.out.println(myCar2.getDriver());

        //Animal
        Animal newAnimal = new Animal();
        newAnimal.call();
        Animal newAnimal2 = new Animal("Nala","black");

        newAnimal2.call();
        System.out.println(newAnimal2.getName());
        System.out.println(newAnimal2.getColor());
        //Set or update

        newAnimal2.setName("luna");
        System.out.println(newAnimal2.getName());
        System.out.println(newAnimal2.numberOfLegs);

        //Dog

        Dog newDog = new Dog();
        newDog.printDogInfo();

//        Dog newDog2 = new Dog("Wawai","black and white", "Aspin");
//        newDog2.printDogInfo();


//        Person newPerson = new Person();
//        newPerson.run();
//        newPerson.sleep();

    }
}
