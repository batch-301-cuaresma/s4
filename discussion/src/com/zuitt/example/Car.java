package com.zuitt.example;

public class Car {
    private  Driver d;
    private  String name;
    private String brand;
    private int yearOfMade;


    //constructors
    public Car(){
        this.d = new Driver("Jhen");
    }

    public Car(String name, String brand, int yearOfMAke){
        this.name = name;
        this.brand = brand;
        this.yearOfMade = yearOfMAke;
        this.d = new Driver("Jhen");
    }

    //setter
    public void setCarName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYearOfMade(int yearOfMade){
        this.yearOfMade = yearOfMade;
    }

    // getters
    public String getCarName(){
        return name;
    }
    public String getBrand(){
        return brand;
    }
    public int getYearOfMade(){
        return yearOfMade;
    }
    public String getDriver(){
        return this.d.getName();
    }

}
