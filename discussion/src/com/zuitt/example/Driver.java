package com.zuitt.example;

public class Driver {
    private String name;

    //setter
    public void setName(String newName){
        this.name = newName;
    }

    //getter
    public String getName(){
        return this.name;
    }
    public Driver(){}
    public  Driver(String name){
        this.name = name;
    }
}
