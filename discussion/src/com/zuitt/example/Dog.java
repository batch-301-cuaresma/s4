package com.zuitt.example;

//[] Inheritance
//"extend" keyword in java is to establish inheritance between classes.
public class Dog extends Animal{
    private String breed;

    // Inheritance is a concept in programming where a class can inherit the properties and methods of another class.
    public Dog(){
        super();  //Anima()
        this.breed = "Chihuahua";
    }
    public Dog(String name,String color, String breed){
        super(name,color);//Animal(String name, String color)
        this.breed = breed;
    }


    //getter
    public String getBreed(){
        return this.breed;
    }

    //setter
    public void setBreed(String breed){
        this.breed = breed;
    }

    public  void printDogInfo(){
        System.out.println("Name: " + getName());
        System.out.println("Color: " + getColor());
        System.out.println("Breed: " + breed);
    }
}
