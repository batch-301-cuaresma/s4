package com.zuitt.example;


import java.util.ArrayList;

public class Phonebook{
    private ArrayList<Contact> contacts;

    public Phonebook(){
        contacts = new ArrayList<>();
    }

    public void addContact(Contact contact){
       contacts.add(contact);
    }
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void displayContacts() {
        if (contacts.isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("-------------------------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("my home town is " +contact.getAddress());
                System.out.println();
            }
        }
    }

}
