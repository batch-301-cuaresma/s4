package com.zuitt.example;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    //Setter
    public void  setName(String name){
        this.name = name;
    }
    public void  setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }
    public void  setAddress(String address){
        this.address = address;
    }

    //getter
    public String getName(){
        return name;
    }
    public String getContactNumber(){
        return contactNumber;
    }
    public String getAddress(){
        return address;
    }

    public Contact(){}
    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }


    public void  call(){
        System.out.println(this.name);
        System.out.println(this.contactNumber);
        System.out.println(this.address);
    }

}
